Feature: Menu

  Background:
    * url 'http://127.0.0.1:8084/'
    * def obj = Java.type('JavaFunctions.WriteInNotepad')

  @CreateMenu
  Scenario Outline:To fetch values from CSV and create customers
    * def RestResponse = call read('Restaurants.feature@GetRestaurantById')
    * def json = read('testData/JsonFiles/CreateMenu.json')
    * print json.restaurant.restaurantName
    * set json.itemName = <name>
    * set json.price = <price>
    * set json.quantity = <quantity>
    * set json.restaurant.restaurantName = RestResponse.response.restaurantName
    * set json.restaurant.restaurantPhoneNumber = RestResponse.response.restaurantPhoneNumber
    * set json.restaurant.restaurantType = RestResponse.response.restaurantType
    * set json.restaurant.id = RestResponse.response.id
    * print json
    Given path 'createMenuItem/'+RestResponse.response.id
    And request json
    When method post
    Then status 200
    And match response.restaurant.restaurantName == RestResponse.response.restaurantName
    And match response.restaurant.restaurantPhoneNumber == RestResponse.response.restaurantPhoneNumber
    And match response.restaurant.restaurantType == RestResponse.response.restaurantType
    And match response.restaurant.id == RestResponse.response.id
    And match response.itemName == <name>
    And match response.price == <price>
    And match response.quantity == <quantity>
    *  def responseData = response
    * obj.storeResponse(responseData,"CreateMenu")



    Examples:
      |read('testData/CsvFiles/CreateMenu.csv')|

  @GetAllMenus
  Scenario:To Get all the menus
    Given path 'getAllMenus'
    When method get
    Then status 200
    *  def responseData = response
    * obj.storeResponse(responseData,"getAllMenus")


  @GetMenuItemByMenuID
  Scenario Outline:To fetch values from CSV and get desired menus
    Given path 'getMenuItemById/'+<id>
    When method get
    Then status 200
    * match response.menu[0].menuId == <id>
    * def responseData = response
    * obj.storeResponse(responseData,"GetMenuItemByMenuID")

    Examples:
      |read('testData/CsvFiles/Getmenu.csv')|


  @GetMenuItemByRestaurantID
  Scenario Outline:To fetch values from CSV and get desired menus
    Given path 'getRestaurantMenu/'+<id>
    When method get
    Then status 200
    * print response
    * def responseData = response
    * obj.storeResponse(responseData,"GetMenuItemByRestaurantID")

    Examples:
      |read('testData/CsvFiles/Getmenu.csv')|

  @RemoveDesiredMenuById
  Scenario Outline:To fetch values from CSV and remove desired restaurants
    Given path 'removeDesiredMenuItem/'+<id>
    When method delete
    Then status 200
    * def responseData = response
    Examples:
      |read('testData/CsvFiles/RemoveMenuItem.csv')|

  @RemoveAllMenu
  Scenario:To Get all the menus
    Given path '/removeAllMenu'
    When method get
    Then status 200
    *  def responseData = response
    * obj.storeResponse(responseData,"/RemoveAllMenu")

  @UpdateMenuItem
  Scenario Outline:To fetch values from CSV and update the Menu

    * def json = read('testData/JsonFiles/UpdateMenuItem.json')
    * set json.itemName = <itemName>
    *  set json.price = <price>
    * set json.quantity = <quantity>
    *  set json.restaurantId = <restaurantId>
    Given path 'updateMenuItem/'+<menuId>
    And request json
    When method post
    Then status 200
    *  def responseData = response
    * obj.storeResponse(responseData,"UpdateMenuItem")

    Examples:
      |read('testData/CsvFiles/UpdateMenuItem.csv')|

  @E2E-Menu
  Scenario Outline:To Perform E2E scenarios for menu

    * def RestResponse = call read('Restaurants.feature@GetRestaurantById')
    * def json = read('testData/JsonFiles/CreateMenu.json')
    * set json.itemName = <name>
    * set json.price = <price>
    * set json.quantity = <quantity>
    * set json.restaurant.restaurantName = RestResponse.response.restaurantName
    * set json.restaurant.restaurantPhoneNumber = RestResponse.response.restaurantPhoneNumber
    * set json.restaurant.restaurantType = RestResponse.response.restaurantType
    * set json.restaurant.id = RestResponse.response.id
    * print json
    Given path 'createMenuItem/'+RestResponse.response.id
    And request json
    When method post
    Then status 200
    And match response.restaurant.restaurantName == RestResponse.response.restaurantName
    And match response.restaurant.restaurantPhoneNumber == RestResponse.response.restaurantPhoneNumber
    And match response.restaurant.restaurantType == RestResponse.response.restaurantType
    And match response.restaurant.id == RestResponse.response.id
    And match response.itemName == <name>
    And match response.price == <price>
    And match response.quantity == <quantity>
    *  def responseData = response
    * obj.storeResponse(responseData,"CreateMenu")

    Given path 'getMenuItemById/'+responseData.id
    When method get
    Then status 200
    * match response.menu[0].menuId == responseData.id
    * match response.menu[0].itemName == responseData.itemName
    * match response.menu[0].price == responseData.price
    * match response.menu[0].quantity == responseData.quantity
    * match response.menu[0].restaurantId == RestResponse.response.id

    * def responseData1 = response
    * obj.storeResponse(responseData1,"GetMenuItemByMenuID")

    * def json1 = read('testData/JsonFiles/UpdateMenuItem.json')
    * set json1.itemName = <UpdatedName>
    *  set json1.price = <Updatedprice>
    * set json1.quantity = <UpdatedQuantity>
    *  set json1.restaurantId = RestResponse.response.id
    Given path 'updateMenuItem/'+responseData.id
    And request json1
    When method post
    Then status 200
    *  def responseData2 = response
    * obj.storeResponse(responseData2,"UpdateMenuItem")

    Given path 'getMenuItemById/'+responseData.id
    When method get
    Then status 200
    * match response.menu[0].menuId == responseData.id
    * match response.menu[0].itemName == <UpdatedName>
    * match response.menu[0].price == <Updatedprice>
    * match response.menu[0].quantity == <UpdatedQuantity>
    * match response.menu[0].restaurantId == RestResponse.response.id

    * def responseData3 = response
    * obj.storeResponse(responseData3,"GetMenuItemByMenuID")

    Given path 'removeDesiredMenuItem/'+responseData.id
    When method delete
    Then status 200
    * def responseData4 = response

    Examples:
      |read('testData/CsvFiles/E2E_Menus.csv')|