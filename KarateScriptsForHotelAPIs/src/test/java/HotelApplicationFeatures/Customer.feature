Feature: Customer

  Background:
    * url 'http://127.0.0.1:8084/'

  @CreateCustomer
  Scenario Outline:To fetch values from CSV and create customers
    * def RestResponse = call read('Restaurants.feature@GetRestaurantById')
    * print RestResponse.response.restaurantName
    * print RestResponse.response.restaurantPhoneNumber
    * def json = read('testData/JsonFiles/CreateCustomer.json')
    * print json.restaurant.restaurantName
    * set json.customerId = <customerId>
    * set json.name = <name>
    * set json.phoneNumber = <phoneNumber>
    * set json.restaurant.restaurantName = RestResponse.response.restaurantName
    * set json.restaurant.restaurantPhoneNumber = RestResponse.response.restaurantPhoneNumber
    * set json.restaurant.restaurantType = RestResponse.response.restaurantType
    * set json.restaurant.id = RestResponse.response.id
    * print json
    Given path 'createNewCustomer'
    And request json
    When method post
    Then status 200


    Examples:
      |read('testData/CsvFiles/CreateCustomer.csv')|