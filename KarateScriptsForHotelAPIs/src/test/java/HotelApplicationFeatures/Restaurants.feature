
Feature: Restaurants functions

  Background:
    * url 'http://127.0.0.1:8084/'
    * def obj = Java.type('JavaFunctions.WriteInNotepad')

@CreateRestaurant
Scenario Outline:To fetch values from CSV and create restaurants

  * def json = read('testData/JsonFiles/CreateRest.json')
  * set json.restaurantId = <restaurantId>
  *  set json.restaurantName = <restaurantName>
  *  set json.restaurantPhoneNumber = <restaurantPhone>
  *  set json.restaurantType = <restaurantType>
  Given path 'createNewRestaurant'
  And request json
  When method post
  Then status 200
  And match response.restaurantName == <restaurantName>
  And match response.restaurantPhoneNumber == <restaurantPhone>
  And match response.restaurantType == <restaurantType>

  *  def responseData = response
  * obj.storeResponse(responseData,"CreateRestaurant")

  Examples:
    |read('testData/CsvFiles/CreateRestaurants.csv')|

  @GetRestaurantById
  Scenario Outline:To fetch values from CSV and get desired restaurants
    Given path 'getDesiredRestaurantById/'+<id>
    When method get
    Then status 200
    And match response.id == <id>
    * def RestaurantByIdResponse = response
    * def responseData = response
    * obj.storeResponse(responseData,"GetRestaurantById")

    Examples:
      |read('testData/CsvFiles/GetRestaurants.csv')|

  @GetAllRestaurants
  Scenario:To Get all the Restaurants
    Given path 'getAllRestaurantslist'
    When method get
    Then status 200
    *  def responseData = response
    * obj.storeResponse(responseData,"GetAllRestaurants")

  @UpdateRestaurant
  Scenario Outline:To fetch values from CSV and update the restaurants

    * def json = read('testData/JsonFiles/UpdateRestaurant.json')
    * set json.restaurantId = <restaurantId>
    *  set json.restaurantName = <restaurantName>
    *  set json.restaurantPhoneNumber = <restaurantPhone>
    *  set json.restaurantType = <restaurantType>
    Given path '/updateRestaurant/'+<restaurantId>
    And request json
    When method post
    Then status 200
    *  def responseData = response
    * obj.storeResponse(responseData,"UpdateRestaurant")

    Examples:
      |read('testData/CsvFiles/UpdateRestaurants.csv')|

  @RemoveRestaurantsById
  Scenario Outline:To fetch values from CSV and remove desired restaurants
    Given path 'removeRestaurantById/'+<id>
    When method delete
    Then status 200
    * def responseData = response
    Examples:
      |read('testData/CsvFiles/GetRestaurants.csv')|

  @E2E-Restaurants
  Scenario Outline:To Perform E2E scenarios for restaurants

    * def json = read('testData/JsonFiles/CreateRest.json')
    *  set json.restaurantName = <restaurantName>
    *  set json.restaurantPhoneNumber = <restaurantPhone>
    *  set json.restaurantType = <restaurantType>
    Given path 'createNewRestaurant'
    And request json
    When method post
    Then status 200
    And match response.restaurantName == <restaurantName>
    And match response.restaurantPhoneNumber == <restaurantPhone>
    And match response.restaurantType == <restaurantType>

    *  def responseData = response
    * obj.storeResponse(responseData,"CreateRestaurant")

    Given path 'getDesiredRestaurantById/'+responseData.id
    When method get
    Then status 200
    And match response.id == responseData.id
    And match response.restaurantName == <restaurantName>
    And match response.restaurantPhoneNumber == <restaurantPhone>
    And match response.restaurantType == <restaurantType>
    * def responseData1 = response
    * obj.storeResponse(responseData1,"GetRestaurantById")

    * def json1 = read('testData/JsonFiles/UpdateRestaurant.json')
    * set json1.restaurantId = responseData.id
    *  set json1.restaurantName = <UpdaterestaurantName>
    *  set json1.restaurantPhoneNumber = <UpdaterestaurantPhone>
    *  set json1.restaurantType = <UpdaterestaurantType>
    Given path '/updateRestaurant/'+responseData.id
    And request json1
    When method post
    Then status 200
    *  def responseData2 = response
    * obj.storeResponse(responseData2,"UpdateRestaurant")

    Given path 'getDesiredRestaurantById/'+responseData.id
    When method get
    Then status 200
    And match response.id == responseData.id
    And match response.restaurantName == <UpdaterestaurantName>
    And match response.restaurantPhoneNumber == <UpdaterestaurantPhone>
    And match response.restaurantType == <UpdaterestaurantType>

    Given path 'removeRestaurantById/'+responseData.id
    When method delete
    Then status 200

    Given path 'getDesiredRestaurantById/'+responseData.id
    When method get
    Then status 404

    Examples:
      |read('testData/CsvFiles/E2E_Restaurants.csv')|


