package JavaFunctions;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.bouncycastle.asn1.tsp.TimeStampReq;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.time.Instant;


public class WriteInNotepad {

    public static void main(String[]args) throws FileNotFoundException, UnsupportedEncodingException {
        timeStamp();
        }
    public static void storeResponse(String response,String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        String timeStamp=timeStamp();
        fileName += timeStamp;
        PrintWriter writer = new PrintWriter(fileName,"UTF-8");
        writer.println("**********Response**************");
        writer.println(response);
        writer.close();

    }

    public static String timeStamp()
    {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String timeStamp=timestamp.toString();
        timeStamp=timeStamp.replace(" ","").toString().trim();
        timeStamp=timeStamp.replace("-",".");
        timeStamp=timeStamp.replace(":",".");
        return timeStamp;

    }
}
