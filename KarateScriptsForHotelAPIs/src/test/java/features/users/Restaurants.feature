Feature: Create Read Update and Delete Restaurants

  Background:
    * url 'http://127.0.0.1:8084/'

  @Smoke
  Scenario:  Create and Delete Restaurants
    * def payloads =
    """
      {
          "restaurantId": 1,
          "restaurantName": "Shanti Sagar",
          "restaurantPhoneNumber": "5555555656",
          "restaurantType": "North Indian Cuisine"
      }
    """
    Given path 'createNewRestaurant'
    And request payloads
    When method post
    Then status 200
    And match response.restaurantName == 'Shanti Sagar'

    * def id = response.id
    * print 'Created ID is ',id

    Given path 'getDesiredRestaurantById/'+id
    When method get
    Then status 200


    * def payload2 =
    """
      {
          "restaurantId": 1,
          "restaurantName": "Shiv Sagar",
          "restaurantPhoneNumber": "3432432433",
          "restaurantType": "Multi Cuisine"
      }
    """
    Given path 'updateRestaurant/'+id
    And request payload2
    When method post
    Then status 200

    Given path 'getDesiredRestaurantById/'+id
    When method get
    Then status 200
    And match response.restaurantName == 'Shiv Sagar'

    Given path 'removeRestaurantById/'+id
    When method delete
    Then status 200

    Given path 'getDesiredRestaurantById/'+id
    When method get
    Then status 404
