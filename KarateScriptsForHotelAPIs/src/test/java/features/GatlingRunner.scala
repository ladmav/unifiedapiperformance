package features

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._

import scala.concurrent.duration._
import scala.language.postfixOps


class GatlingRunner extends Simulation{

  var sce = scenario("Restaurants").exec(karateFeature("classpath:HotelApplicationFeatures/Restaurants.feature","@E2E-Restaurants"))
  var sce1 = scenario("menus").exec(karateFeature("classpath:HotelApplicationFeatures/Menu.feature","@E2E-Menu"))

  setUp(
    sce.inject(rampUsers(5). during (5 seconds)),
    sce1.inject(rampUsers(5). during (5 seconds))
  )
}
