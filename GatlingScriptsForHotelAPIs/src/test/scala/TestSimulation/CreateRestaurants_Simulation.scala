package TestSimulation

import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._
import io.gatling.core.Predef._
import scala.concurrent.duration._


class CreateRestaurants_Simulation  extends Simulation{

  val httpProtocol = http.baseUrl("http://127.0.0.1:8084/")

  val scn = scenario("Scenario")
    .exec(http("CreateRestaurants")
      .post("createNewRestaurant")
      .body(RawFileBody("./src/test/payload/createRestaurant.json")).asJson)



  setUp(scn.inject(rampUsers(5).during(5))
    .protocols(httpProtocol))

}
