package TestSimulation

import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._
import io.gatling.core.Predef._


class GetAllRestaurants_Simulation  extends Simulation{

  val httpProtocol = http.baseUrl("http://127.0.0.1:8084/")

  val scn = scenario("Scenario")
    .exec(http("GetAllRestauratns")
    .get("getAllRestaurantslist"))

    setUp(scn.inject(atOnceUsers(1000)).protocols(httpProtocol))

}
