package jobtest;

import com.intuit.karate.gatling.GatlingMavenJobConfig;
import com.intuit.karate.job.JobExecutor;
import com.intuit.karate.job.JobManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pthomas3
 */
public class GatlingRunner2 {

    public static void main(String[] args) {
        try{
        int executorCount = 2;
                ExecutorService executor = Executors.newFixedThreadPool(executorCount);
                for (int i = 0; i < executorCount; i++) {
                    executor.submit(() -> JobExecutor.run("http://192.168.0.150:8087"));
                }
                executor.shutdown();
                try {
                    executor.awaitTermination(0, TimeUnit.MINUTES);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }catch(Exception e){

        }

    }

}
